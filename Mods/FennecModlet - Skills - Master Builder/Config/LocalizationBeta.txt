Key,File,Type,UsedInMainMenu,NoTranslate,english
modUpgradeExtenderT1FM,item_modifiers,Mod,,,"Basic Range Extender (3x3) Mod"
modUpgradeExtenderT1FMDesc,item_modifiers,Mod,,,"Install this on any repair tool like a hammer or nailgun to allow it to upgrade blocks in a 3x3 area."
modUpgradeExtenderT2FM,item_modifiers,Mod,,,"Advanced Range Extender (5x5) Mod"
modUpgradeExtenderT2FMDesc,item_modifiers,Mod,,,"Install this on any repair tool like a hammer or nailgun to allow it to upgrade blocks in a 5x5 area."
modHitReducerFM,item_modifiers,Mod,,,"Upgrade Hit Reducer Mod"
modHitReducerFMDesc,item_modifiers,Mod,,,"Install this on any repair tool like a hammer or nailgun to reduce the number of hits it takes to upgrade a block."
modHitSpeedIncreaserFM,item_modifiers,Mod,,,"Upgrade Speed Increaser Mod"
modHitSpeedIncreaserFMDesc,item_modifiers,Mod,,,"Install this on any repair tool like a hammer or nailgun to increase the speed of block upgrades."
perkMasterBuilderFMName,progression,Perk  Int,,,"Master Builder"
perkMasterBuilderFMDesc,progression,Perk  Int,,,"Learn how to build more efficiently by upgrading your construction tools with specialized mods."
perkMasterBuilderFMRank1Desc,progression,Perk  Int,,,"Bricklayer"
perkMasterBuilderFMRank2Desc,progression,Perk  Int,,,"Construction Manager"
perkMasterBuilderFMRank3Desc,progression,Perk  Int,,,"Foreman"
perkMasterBuilderFMRank4Desc,progression,Perk  Int,,,"Master Builder"
perkMasterBuilderFMRank1LongDesc,progression,Perk  Int,,,"You are laying the foundations for some big projects and are becoming familiar with construction tools. Craft Upgrade Speed Increaser mods for your tools that reduce how much time it takes to upgrade blocks.
perkMasterBuilderFMRank2LongDesc,progression,Perk  Int,,,"Building large constructions can be tricky, especially if upgrading takes a long time. Fortunately, you have a secret up your sleeve. Craft Basic Range Extender mods for your construction tools that allow you to upgrade blocks in a 3x3x3 area." 
perkMasterBuilderFMRank3LongDesc,progression,Perk  Int,,,"As the head honcho of the building world, you know how to get things done. Craft Upgrade Hit Reducer mods that reduce the number of hits needed to upgrade a block."
perkMasterBuilderFMRank4LongDesc,progression,Perk  Int,,,"You are a master builder and can have structures up in minutes. Craft Enhanced Range Extender mods for your construction tools that allow you to upgrade blocks in a 5x5x5 area."
statHitRangeFM,ui_display,Stat,,,"Range"
statHitsReducedFM,ui_display,Stat,,,"Hits Reduced"
statSpeedIncreaseFM,ui_display,Stat,,,"Speed Increase"
