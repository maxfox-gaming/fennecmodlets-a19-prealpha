Key,Source,Context,Changes,English
perkCorpseHarvestFMName,progression,Perk,New,"Corpse Harvester"
perkCorpseHarvestFMDesc,progression,Perk,New,"Those zombies you kill can provide a great source of materials, if you know where to look..."
perkCorpseHarvestFMRank1Desc,progression,Perk,New,"Pocket Snatcher"
perkCorpseHarvestFMRank2Desc,progression,Perk,New,"Cadaver Chopper"
perkCorpseHarvestFMRank3Desc,progression,Perk,New,"Mortician"
perkCorpseHarvestFMRank4Desc,progression,Perk,New,"Carcass Crusher"
perkCorpseHarvestFMRank5Desc,progression,Perk,New,"Corpse Harvester"
perkCorpseHarvestFMRank1LongDesc,progression,Perk,New,"Zombies may be disgusting, but cutting them up can yield some extra junk. Get a small chance of getting extra junk from zombie remains."
perkCorpseHarvestFMRank2LongDesc,progression,Perk,New,"Got used to the smell of the dead? Good. Get a small chance of getting extra junk from corpses, as well as some more valuable items."
perkCorpseHarvestFMRank3LongDesc,progression,Perk,New,"Why just take the junk off zombies? Cutting their rags off carefully might yield you more useful items. Get a small chance of getting extra junk, clothes, and some more valuable items from corpses."
perkCorpseHarvestFMRank4LongDesc,progression,Perk,New,"The art of undead choppage is becoming second nature to you. Get a small chance of finding clothes, more valuable items, and even more junk from corpses. Occasionally find useful items such as food, medical items, and firearms."
perkCorpseHarvestFMRank5LongDesc,progression,Perk,New,"Those corpses are just waiting to be plundered. You now have a small chance to find extremely valuable items from corpses and an even greater chance to find everything else."