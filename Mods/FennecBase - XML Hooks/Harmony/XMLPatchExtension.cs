﻿using System;
using System.Xml;
using System.Reflection;
using HarmonyLib;

/**
 * Harmony patch that allows the XUiC to read icons for the new power sources.
 */

[HarmonyPatch]
public class XMLPatchExtension
{
    private static MethodBase TargetMethod()
    {
        // refer to C# reflection documentation:
        return typeof(XmlPatcher).GetMethod("singlePatch", BindingFlags.Static | BindingFlags.NonPublic);
    }
    public static bool Prefix(XmlFile _targetFile, XmlElement _patchElement, string _patchName, ref bool __result) 
    {
        __result = false;
        string name = _patchElement.Name;
        if (!_patchElement.HasAttribute("xpath"))
        {
            throw new Exception("XML.Patch (" + _patchElement.GetXPath() + "): Patch element does not have an 'xpath' attribute");
        }
        string attribute = _patchElement.GetAttribute("xpath");

        if (name == "removeattributepartial")
        {
            __result = _targetFile.RemoveAttributePartialByXPath(attribute, _patchElement, _patchName) > 0;
            return false;
        }
        else if (name == "removetagsfromattribute")
        {    
            __result = _targetFile.RemoveTagsFromAttrubuteByXPath(attribute, _patchElement, _patchName) > 0;
            return false;
        }
        else if (name == "attributemath")
        {
            __result = _targetFile.AttrubuteMathByXPath(attribute, _patchElement, _patchName) > 0;
            return false;
        }
        else if (name == "insertAfterIterate")
        {
            __result = _targetFile.InsertAfterIterateByXPath(attribute, _patchElement, _patchName) > 0;
            return false;
        }
        
        return true;
    }
}
