﻿using System;
using System.Collections.Generic;

public class BlockMultiblockMasterTransformer : BlockLoot, IBlockTransformer, IBlockMultiblockComponent
{
	/**
	 *	Called when the block is loaded. This block has a tile entity attached, so need to set this to true.
	 */

    public BlockMultiblockMasterTransformer()
    {
        this.HasTileEntity = true;
    }


    /**
     * When block is initialised
     */

    public override void Init()
    {
        base.Init();
        if (!this.Properties.Values.ContainsKey("Structure"))
        {
            throw new Exception("Multiblock " + this.GetBlockName() + " does not contain a Structure property.");
        }
        this.Structure = this.Properties.Values["Structure"];

        this.WorkstationData = new WorkstationData(base.GetBlockName(), this.Properties);
        CraftingManager.AddWorkstationData(this.WorkstationData);

        MultiblockData multiblockData = new MultiblockData(this.Structure, this.GetBlockName());
        MultiblockManager.AddMultiblock(multiblockData);
    }


    /**
     * After initial block values loaded, this will load the extra properties for us.
     */

    public override void LateInit()
    {
        base.LateInit();
        this.transformationPropertyParser = new TransformationPropertyParser(this);
        this.transformationPropertyParser.ParseDynamicProperties();
        this.transformationPropertyParser.collection.AddEnabledRecipes();
        MultiblockManager.ValidateMultiblock(this.GetBlockName());
    }


    /**
     * This happens when the block is placed in the world.
     */

    public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
    {
        Block block = Block.list[_result.blockValue.type];
        if (block.shape.IsTerrain())
        {
            _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, this.Density);
        }
        else if (!block.IsTerrainDecoration)
        {
            _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, MarchingCubes.DensityAir);
        }
        else
        {
            _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue);
        }

        TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = _world.GetTileEntity(_result.clrIdx, _result.blockPos) as TileEntityMultiblockMasterTransformer;
        if (tileEntityBlockTransformer == null)
        {
            Log.Warning("Failed to create tile entity");
            return;
        }
        if (_ea != null && _ea.entityType == EntityType.Player)
        {
            tileEntityBlockTransformer.bPlayerStorage = true;
            tileEntityBlockTransformer.worldTimeTouched = _world.GetWorldTime();
            tileEntityBlockTransformer.SetEmpty();
        }
    }


    /**
     * This is the activation text that displays when the player looks at this block.
     * It will display whether the block is working, and if not, what conditions need to be fulfilled to make it work.
     */

    public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
    {
        TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntityMultiblockMasterTransformer;
        if (tileEntityBlockTransformer == null)
        {
            return string.Empty;
        }
        string lBlockName = Localization.Get(Block.list[_blockValue.type].GetBlockName());
        PlayerActionsLocal playerInput = ((EntityPlayerLocal)_entityFocusing).playerInput;
        string playerKey = playerInput.Activate.GetBindingXuiMarkupString(XUiUtils.EmptyBindingStyle.EmptyString, XUiUtils.DisplayStyle.Plain, null) + playerInput.PermanentActions.Activate.GetBindingXuiMarkupString(XUiUtils.EmptyBindingStyle.EmptyString, XUiUtils.DisplayStyle.Plain, null);

        string tooltip = "";
        if (!tileEntityBlockTransformer.MultiblockFormed())
        {
            return Localization.Get("ttMultiblockNotFormed");
        }


        // If updates cannot happen we need to display this to the user and say why they can't happen yet.
        if (!tileEntityBlockTransformer.UpdateCanHappen((World)_world))
        {
            // If no power, display this when looking at the block.
            if (!tileEntityBlockTransformer.IsPowered(_world as World))
            {
                tooltip = Localization.Get("transformerMultiblockTooltipNoPower");
                return string.Format(tooltip, playerKey, lBlockName);
            }

            // If no heat, display this when looking at the block.
            else if (!tileEntityBlockTransformer.IsHeated(_world as World))
            {
                tooltip = Localization.Get("transformerTooltipNoHeat");
                List<string> lHeatSources = new List<string>();
                if (this.transformationPropertyParser.heatSources.Count > 0)
                {
                    foreach (string heatSource in this.transformationPropertyParser.heatSources)
                    {
                        lHeatSources.Add(Localization.Get(heatSource));
                    }
                }
                string sources = StringHelpers.WriteListToString(lHeatSources);
                return string.Format(tooltip, playerKey, lBlockName, sources);
            }

            // Checks we have nearby blocks
            else if (!tileEntityBlockTransformer.HasNearbyBlocks(_world as World))
            {
                tooltip = Localization.Get("transformerTooltipNoBlocksNearby");
                List<string> lNearbyBlocks = new List<string>();
                if (this.transformationPropertyParser.nearbyBlockNames.Count > 0)
                {
                    foreach (string blockName in this.transformationPropertyParser.nearbyBlockNames)
                    {
                        lNearbyBlocks.Add(Localization.Get(blockName));
                    }
                }

                if (this.transformationPropertyParser.nearbyBlockTags.Count > 0)
                {
                    foreach (string blockTag in this.transformationPropertyParser.nearbyBlockTags)
                    {
                        lNearbyBlocks.Add(blockTag);
                    }
                }

                string sources = StringHelpers.WriteListToString(lNearbyBlocks);
                string needed = this.transformationPropertyParser.nearbyBlockCount.ToString();

                return string.Format(tooltip, playerKey, lBlockName, needed, sources);
            }

            // If all else fails, the block must be empty so needs items.
            else
            {
                tooltip = Localization.Get("transformerTooltipReady");
                return string.Format(tooltip, playerKey, lBlockName);
            }
        }

        // Transforming here...
        tooltip = Localization.Get("transformerTooltipWorking");
        return string.Format(tooltip, playerKey, lBlockName);
    }


    /**
     * When block is added to the game.
     */

    public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
    {
        if (_blockValue.ischild)
        {
            return;
        }
        this.shape.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
        if (this.isMultiBlock)
        {
            this.multiBlockPos.AddChilds(world, _chunk, _chunk.ClrIdx, _blockPos, _blockValue);
        }

        if (this.lootList >= LootContainer.lootList.Length || LootContainer.lootList[this.lootList] == null)
        {
            Log.Error("No loot entry defined for loot list id {0}", new object[]
            {
            this.lootList
            });
            return;
        }
        this.addTileEntity(world, _chunk, _blockPos, _blockValue);
    }


    /**
     * This is called when block is removed. We want to destroy existing tile entity too.
     */

    public override void OnBlockRemoved(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
    {
        if (!_blockValue.ischild)
        {
            this.shape.OnBlockRemoved(_world, _chunk, _blockPos, _blockValue);
            if (this.isMultiBlock)
            {
                this.multiBlockPos.RemoveChilds(_world, _chunk.ClrIdx, _blockPos, _blockValue);
                return;
            }
        }
        else if (this.isMultiBlock)
        {
            this.multiBlockPos.RemoveParentBlock(_world, _chunk.ClrIdx, _blockPos, _blockValue);
        }

        TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = _world.GetTileEntity(_chunk.ClrIdx, _blockPos) as TileEntityMultiblockMasterTransformer;
        if (tileEntityBlockTransformer != null)
        {
            tileEntityBlockTransformer.OnDestroy();
        }
        this.removeTileEntity(_world, _chunk, _blockPos, _blockValue);
    }


    /**
     * Helper method to add and set up a tile entity for the block.
     */

    protected override void addTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
    {
        TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = new TileEntityMultiblockMasterTransformer(_chunk);
        tileEntityBlockTransformer.localChunkPos = World.toBlock(_blockPos);
        tileEntityBlockTransformer.lootListIndex = (int)((ushort)this.lootList);
        tileEntityBlockTransformer.SetContainerSize(LootContainer.lootList[this.lootList].size, true);
        tileEntityBlockTransformer.SetTransformationCollection(this.transformationPropertyParser.collection);
        tileEntityBlockTransformer.SetRequirePower(
            this.transformationPropertyParser.requirePower, 
            this.transformationPropertyParser.powerSources
        );
        tileEntityBlockTransformer.SetRequireHeat(
            this.transformationPropertyParser.requireHeat, 
            this.transformationPropertyParser.heatSources
        );
        tileEntityBlockTransformer.SetRequireNearbyBlocks(
            this.transformationPropertyParser.requireNearbyBlocks,
            this.transformationPropertyParser.nearbyBlockNames,
            this.transformationPropertyParser.nearbyBlockTags,
            this.transformationPropertyParser.requireAllTags,
            this.transformationPropertyParser.nearbyBlockRange,
            this.transformationPropertyParser.nearbyBlockCount
        );
        tileEntityBlockTransformer.SetRequireUserAccess(
            this.transformationPropertyParser.requireUserAccess
        );
        tileEntityBlockTransformer.CalculateLookupCoordinates();
        tileEntityBlockTransformer.SetMasterBlockName(this.GetBlockName());
        _chunk.AddTileEntity(tileEntityBlockTransformer);

    }

    
    /**
     * Helper method to remove a tile entity.
     */

    protected override void removeTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
    {
        _chunk.RemoveTileEntityAt<TileEntityMultiblockMasterTransformer>((World)world, World.toBlock(_blockPos));
    }

    
    /**
     * Called when a block is destroyed by an entity. It cam be useful for separating things out if needed depending on what
     * entity destroyed it.
     */

    public override Block.DestroyedResult OnBlockDestroyedBy(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, int _entityId, bool _bUseHarvestTool)
    {
        TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntityMultiblockMasterTransformer;
        if (tileEntityBlockTransformer != null)
        {
            tileEntityBlockTransformer.OnDestroy();
        }
        LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(GameManager.Instance.World.GetEntity(_entityId) as EntityPlayerLocal);
        if (null != uiforPlayer && uiforPlayer.windowManager.IsWindowOpen("looting") && ((XUiC_LootWindow)uiforPlayer.xui.GetWindow("windowLooting").Controller).GetLootBlockPos() == _blockPos)
        {
            uiforPlayer.windowManager.Close("looting");
        }
        if (tileEntityBlockTransformer != null)
        {
            _world.GetGameManager().DropContentOfLootContainerServer(_blockValue, _blockPos, tileEntityBlockTransformer.entityId);
        }
        return Block.DestroyedResult.Downgrade;
    }


    /**
     * If multiblock is not formed properly will display a message.
     * Otherwise will open the inventory.
     */

    public override bool OnBlockActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_player.inventory.IsHoldingItemActionRunning())
        {
            return false;
        }
        TileEntityMultiblockMasterTransformer tileEntityTransformer = _world.GetTileEntity(_cIdx, _blockPos) as TileEntityMultiblockMasterTransformer;
        if (tileEntityTransformer == null)
        {
            return false;
        }

        if (!tileEntityTransformer.MultiblockFormed())
        {
            EntityPlayerLocal _playerLocal = _player as EntityPlayerLocal;
            if (_playerLocal != null)
            {
                GameManager.ShowTooltipWithAlert(_playerLocal as EntityPlayerLocal, Localization.Get("ttMultiblockNotFormed"), "ui_denied");
            }
            return false;
        }

        _player.AimingGun = false;
        Vector3i blockPos = tileEntityTransformer.ToWorldPos();
        tileEntityTransformer.bWasTouched = tileEntityTransformer.bTouched;
        _world.GetGameManager().TELockServer(_cIdx, blockPos, tileEntityTransformer.entityId, _player.entityId, null);
        return true;
    }


    /**
     * Gets activation based on command when you hold E
     */

    public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        return _indexInBlockActivationCommands == 0 && this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
    }


    /**
     * Returns the commands to activate the block looking at.
     */

    private BlockActivationCommand[] cmds = new BlockActivationCommand[]
    {
        new BlockActivationCommand("Search", "search", true) 
    };


    public WorkstationData WorkstationData;

    // Transformation properties to pass on to the tile entity.
    private TransformationPropertyParser transformationPropertyParser;
    private string Structure;
}
