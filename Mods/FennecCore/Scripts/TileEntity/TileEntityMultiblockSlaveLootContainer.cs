﻿using System.Collections.Generic;

// Token: 0x020008BA RID: 2234
public class TileEntityMultiblockSlaveLootContainer : TileEntityLootContainer, IInventory, ITileEntityMultiblockSlaveInventory
{
    /**
     * Constructor
     */

    public TileEntityMultiblockSlaveLootContainer(Chunk _chunk) : base(_chunk)
    {
        this.containerSize = new Vector2i(3, 3);
        this.lootListIndex = 0;
    }


    /**
     * Clones the loot container from another one.
     */

    private TileEntityMultiblockSlaveLootContainer(TileEntityMultiblockSlaveLootContainer _other) : base(null)
    {
        this.lootListIndex      = _other.lootListIndex;
        this.containerSize      = _other.containerSize;
        this.items              = ItemStack.Clone(_other.items);
        this.bTouched           = _other.bTouched;
        this.worldTimeTouched   = _other.worldTimeTouched;
        this.bPlayerBackpack    = _other.bPlayerBackpack;
        this.bPlayerStorage     = _other.bPlayerStorage;
        this.bUserAccessing     = _other.bUserAccessing;
        this.type               = _other.type;
    }


    /**
     * Clones the container
     */

    public new TileEntityMultiblockSlaveLootContainer Clone()
    {
        return new TileEntityMultiblockSlaveLootContainer(this);
    }


    /**
     * Gets the master block if there is one. Useful for when you are interacting with multiblock via its slave.
     */

    public ITileEntityMultiblockMaster GetMaster()
    {
        return this.master;
    }


    /**
     * Sets the master block for this slave block. Slave blocks can only ever have one master block at a time.
     */

    public void SetMaster(ITileEntityMultiblockMaster master)
    {
        if (this.HasMaster)
        {
            return;
        }
        this.master = master;
    }


    /**
     * Unsets the master for this tile entity.
     */

    public void UnsetMaster()
    {
        this.master = null;
    }


    /**
     * Sets the type of this slave (useful for defining types rather than using block names where substitutes are possible
     */

    public void SetSlaveType(string type)
    {
        this.type = type;
    }


    /**
     * Gets the type of the slave, or the block name if it was not defined before.
     */

    public string GetSlaveType()
    {
        if (this.type != null)
        {
            return this.type;
        }

        World world = GameManager.Instance.World;
        return world != null ? world.GetBlock(this.ToWorldPos()).Block.GetBlockName() : null;
    }


    /**
     * Removes the master block binding
     */

    public void UnbindMaster()
    {
        if (this.HasMaster)
        {
            this.master = null;
        }
    }


    /**
     * If this slave block hsa been destroyed and it had a master block, the multiblock will no longer be formed correctly so notify the master block.
     */

    public override void OnRemove(World world)
    {
        base.OnRemove(world);
        if (this.HasMaster)
        {
            this.master.SetMultiblockNotFormed();
        }
        this.OnDestroy();
    }


    /**
     * Reads the type of the multiblock
     */

    public override void read(PooledBinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
    {
        base.read(_br, _eStreamMode);
        this.type = _br.ReadString();
    }


    /**
     * Writes out tile entity data to save file.
     */

    public override void write(PooledBinaryWriter stream, TileEntity.StreamModeWrite _eStreamMode)
    {
        base.write(stream, _eStreamMode);
        stream.Write(this.type);
    }


    /**
     * Gets the tile entity type
     */

    public override TileEntityType GetTileEntityType()
    {
        return TileEntityType.MultiblockSlaveLoot;
    }
    

    /**
     * When something happens to the tile entity.
     */

    private void tileEntityChanged()
    {
        for (int i = 0; i < this.listeners.Count; i++)
        {
            this.listeners[i].OnTileEntityChanged(this, 0);
        }
    }

    
    private Vector2i containerSize;
    private ItemStack[] itemsArr;
    private List<Entity> entList;

    public bool IsPlayerPlaced = false;
    private ITileEntityMultiblockMaster master;
    private string type;
    public bool HasMaster
    {
        get { return (this.master != null); }
    }
}
