﻿using System.Collections.Generic;
using UnityEngine;
using Audio;

public class TileEntityMultiblockSlavePowered : TileEntityPowered, IPowered, ITileEntityMultiblockSlavePowered
{
	/**
	 * Constructor
	 */

	public TileEntityMultiblockSlavePowered(Chunk _chunk) : base(_chunk)
	{
	}


    /**
     * Gets the master block if there is one. Useful for when you are interacting with multiblock via its slave.
     */

    public ITileEntityMultiblockMaster GetMaster()
    {
        return this.master;
    }


    /**
     * Sets the master block for this slave block. Slave blocks can only ever have one master block at a time.
     */

    public void SetMaster(ITileEntityMultiblockMaster master)
    {
        if (this.HasMaster)
        {
            return;
        }
        this.master = master;
    }


    /**
     * Unsets the master for this tile entity.
     */

    public void UnsetMaster()
    {
        this.master = null;
    }


    /**
     * Sets the type of this slave (useful for defining types rather than using block names where substitutes are possible
     */

    public void SetSlaveType(string type)
    {
        this.type = type;
    }


    /**
     * Gets the type of the slave, or the block name if it was not defined before.
     */

    public string GetSlaveType()
    {
        if (this.type != null)
        {
            return this.type;
        }

        World world = GameManager.Instance.World;
        return world != null ? world.GetBlock(this.ToWorldPos()).Block.GetBlockName() : null;
    }


    /**
     * Removes the master block binding
     */

    public void UnbindMaster()
    {
        if (this.HasMaster)
        {
            this.master = null;
        }
    }


    /**
     * If this slave block hsa been destroyed and it had a master block, the multiblock will no longer be formed correctly so notify the master block.
     */

    public override void OnRemove(World world)
    {
        base.OnRemove(world);
        if (this.HasMaster)
        {
            this.master.SetMultiblockNotFormed();
        }
        this.OnDestroy();
    }


    /**
     * Unload method (Needs to get around nullref)
     */

    public override void OnUnload(World world)
    {
        if (SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
        {
            if (this.PowerItem != null)
            {
                this.PowerItem.RemoveTileEntity(this);
            }
        }
        this.RemoveWires();
    }


    /**
     * Reads the type of the multiblock
     */

    public override void read(PooledBinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
    {
        base.read(_br, _eStreamMode);
        this.type = _br.ReadString();
    }


    /**
     * Writes out tile entity data to save file.
     */

    public override void write(PooledBinaryWriter stream, TileEntity.StreamModeWrite _eStreamMode)
    {
        base.write(stream, _eStreamMode);
        stream.Write(this.type);
    }


    /**
     * Returns the tile entity type, used for instantiating the object.
     */

    public override TileEntityType GetTileEntityType()
    {
        return TileEntityType.MultiblockSlavePowered;
    }


    /**
     * What happens when the block is upgraded/downgraded
     */

    public override void UpgradeDowngradeFrom(TileEntity _other)
    {
        base.UpgradeDowngradeFrom(_other);
        this.OnDestroy();
    }


    /**
     * What happens when the tile entity is changed in any way.
     */

    private void tileEntityChanged()
    {
        for (int i = 0; i < this.listeners.Count; i++)
        {
            this.listeners[i].OnTileEntityChanged(this, 0);
        }
    }


    /**
     * Resets all tile entity data.
     */

    public override void Reset()
    {
        base.Reset();
        this.setModified();
    }


    public bool IsPlayerPlaced = false;

    private ITileEntityMultiblockMaster master;
    private string type;

    public bool HasMaster
    {
        get { return (this.master != null); }
    }



    private bool needBlockData;
	private int requiredPower;
	private bool isPowered;
	private Transform blockTransform;
	private List<IWireNode> currentWireNodes = new List<IWireNode>();
	private List<Vector3i> wireDataList = new List<Vector3i>();
	private bool activateDirty;
	private Vector3i parentPosition = new Vector3i(-9999, -9999, -9999);
}
