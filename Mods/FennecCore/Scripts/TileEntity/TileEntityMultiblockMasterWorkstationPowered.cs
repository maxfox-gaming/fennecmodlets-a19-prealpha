﻿using System.Collections.Generic;
using UnityEngine;

public class TileEntityMultiblockMasterWorkstationPowered : TileEntityMultiblockMasterWorkstation, ITileEntityMultiblockMaster, ITileEntityReceivePower
{
    public new event XUiEvent_InputStackChanged InputChanged;
    public new event XUiEvent_OutputStackChanged OutputChanged;
    public new event XUiEvent_FuelStackChanged FuelChanged;
    public new event XUiEvent_ToolStackChanged ToolChanged;


    /**
    * Constructor
    */

    public TileEntityMultiblockMasterWorkstationPowered(Chunk _chunk) : base(_chunk)
    {
        this.fuel = ItemStack.CreateArray(3);
        this.tools = ItemStack.CreateArray(3);
        this.output = ItemStack.CreateArray(6);
        this.input = ItemStack.CreateArray(3);
        this.lastInput = ItemStack.CreateArray(3);
        this.queue = new RecipeQueueItem[4];
        this.materialNames = new string[0];
        this.isModuleUsed = new bool[5];
        this.currentMeltTimesLeft = new float[this.input.Length];
        this.dataObject = 0;
        this.hasPower = false;
        this.poweredSlaves = new Dictionary<Vector3i, Block>();
    }


    /**
     * Checks if block is powered. It is required to be next to a TileEntityPowered type of block in any cardinal direction.
     */

    public bool IsPowered(World world)
    {
        if (!this.MultiblockFormed())
        {
            this.hasPower = false;
            this.setModified();
            return this.hasPower;
        }

        if (this.poweredSlaves == null || this.poweredSlaves.Count == 0)
        {
            this.hasPower = true;
            this.setModified();
            return this.hasPower;
        }

        if (!this.requiresPower)
        {
            this.hasPower = true;
            this.setModified();
            return this.hasPower;
        }

        int flag = 0;
        foreach (KeyValuePair<Vector3i, Block> entry in poweredSlaves)
        {
            Chunk chunk = world.GetChunkFromWorldPos(entry.Key) as Chunk;
            Vector3i pos = Chunk.ToLocalPosition(entry.Key);
            if (chunk == null)
            {
                continue;
            }

            Block block = world.GetBlock(entry.Key).Block;
            if (entry.Value.GetBlockName() != block.GetBlockName())
            {
                this.SetMultiblockNotFormed();
                this.hasPower = false;
                this.setModified();
                return this.hasPower;
            }

            if (!block.HasTileEntity)
            {
                this.SetMultiblockNotFormed();
                this.hasPower = false;
                this.setModified();
                return this.hasPower;
            }

            TileEntityPowered te = chunk.GetTileEntity(pos) as TileEntityPowered;
            if (te == null)
            {
                continue;
            }

            if (te.IsPowered)
            {
                flag += 1;
            }
        }

        this.hasPower = (flag >= poweredSlaves.Count);
        this.setModified();
        return this.hasPower;
    }


    /**
     * Updates the master block
     */

    public override bool UpdateMaster(World world, bool checkRotationIndex = false)
    {
        // If master block not found already, attempt to get it.
        if (this.masterBlockName == null)
        {
            Vector3i worldPos = this.ToWorldPos();
            Block block = world.GetBlock(worldPos).Block;
            if (block == null)
            {
                return false;
            }

            this.masterBlockName = block.GetBlockName();
        }

        // Need to check each rotation in turn to make sure that the structure is formed.
        for (int i = 0; i < 4; i += 1)
        {
            if (checkRotationIndex)
            {
                if (this.rotationIndex == -1)
                {
                    UpdateMaster(world);
                    return false;
                }

                if (this.rotationIndex != i)
                {
                    continue;
                }
            }

            Dictionary<Vector3i, string> multiblockStructure = MultiblockManager.GetCoordinatesInWorldSpace(this.masterBlockName, this.ToWorldPos(), i);
            List<ITileEntityMultiblockSlave> slaveBlocks = new List<ITileEntityMultiblockSlave>();
            Dictionary<Vector3i, Block> poweredSlaves = new Dictionary<Vector3i, Block>();
            foreach (KeyValuePair<Vector3i, string> blockData in multiblockStructure)
            {
                Vector3i blockPosition = blockData.Key;
                string blockNameType = blockData.Value;
                Block block = world.GetBlock(blockPosition).Block;
                
                if (!block.HasTileEntity)
                {
                    continue;
                }

                Chunk chunk = world.GetChunkFromWorldPos(blockPosition) as Chunk;
                if (chunk == null)
                {
                    continue;
                }

                ITileEntityMultiblockSlave te = chunk.GetTileEntity(Chunk.ToLocalPosition(blockPosition)) as ITileEntityMultiblockSlave;
                if (te == null) //Must be a powered one that's inactive.
                {
                    poweredSlaves.Add(blockPosition, block);
                    continue;
                }

                string slaveType = te.GetSlaveType();
                if (slaveType == null || slaveType != blockNameType)
                {
                    break;
                }
                slaveBlocks.Add(te);
            }

            if ((slaveBlocks.Count + poweredSlaves.Count) < multiblockStructure.Count)
            {
                continue;
            }

            this.activeSlaveBlocks = slaveBlocks;
            this.poweredSlaves = poweredSlaves;
            foreach (ITileEntityMultiblockSlave slaveBlock in this.activeSlaveBlocks)
            {
                slaveBlock.SetMaster(this);
            }

            this.SetMultiblockFormed();
            this.rotationIndex = i;
            return true;
        }
        this.rotationIndex = -1;
        this.SetMultiblockNotFormed();
        return false;
    }



    /**
     * Clears slave data from the block
     */

    protected override void ClearSlaveData()
    {
        base.ClearSlaveData();
        this.poweredSlaves = null;
    }


    /**
     * Loads data from save file and reads into the tile entity.
     */

    public override void read(PooledBinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
    {
        base.read(_br, _eStreamMode);
        this.requiresPower = _br.ReadBoolean();
        this.hasPower = false;
    }


    /**
     * Writes out the required data to the tile entity.
     */

    public override void write(PooledBinaryWriter _bw, TileEntity.StreamModeWrite _eStreamMode)
    {
        base.write(_bw, _eStreamMode);
        _bw.Write(this.requiresPower);
    }


    /**
     * Sets required power.
     */

    public void SetRequirePower(bool requirePower)
    {
        this.requiresPower = requirePower;
    }

    /**
     * Returns WorkstationPowered as a tile entity type.
     */

    public override TileEntityType GetTileEntityType()
    {
        return TileEntityType.MultiblockMasterWorkstationPowered;
    }



    private const float cFuelBurnPerTick = 1f;
    private ItemStack[] fuel;
    private ItemStack[] input;
    private ItemStack[] tools;
    private ItemStack[] output;
    private RecipeQueueItem[] queue;
    private ulong lastTickTime;
    private int fuelInStorageInTicks;
    private float currentBurnTimeLeft;
    private float[] currentMeltTimesLeft;
    private ItemStack[] lastInput;
    private bool isBurning;
    private bool isBesideWater;
    private bool isPlayerPlaced;
    private string[] materialNames;
    private bool[] isModuleUsed;
    private int Debug = 1;

    private enum Module
    {
        Tools,
        Input,
        Output,
        Fuel,
        Material_Input,
        Count
    }

    private int debug
    {
        get
        {
            int current = this.Debug;
            this.Debug -= 1;
            return (current > 0 ? current : 0);
        }
    }

    private int rotationIndex;
    private string masterBlockName;

    // Saved variables that are called on Write() and Read() methods.
    private bool requiresPower;
    
    private List<ITileEntityMultiblockSlave> activeSlaveBlocks;
    private Dictionary<Vector3i, Block> poweredSlaves;

    // These store whether the block is powered, heated and has nearby blocks, to reduce amount of calls to check.
    private bool hasPower;
}
