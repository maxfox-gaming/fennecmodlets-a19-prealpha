﻿public interface IBlockTransformer
{
    string GetBlockName();
}