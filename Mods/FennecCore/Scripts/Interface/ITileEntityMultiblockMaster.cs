﻿using System.Collections.Generic;

public interface ITileEntityMultiblockMaster
{
    /**
     * Sets the name of the block
     */

    void SetMasterBlockName(string name);


    /**
     * Notifies the master to send a server update.
     */

    void TriggerUpdate();

    
    /**
     * Updates the master block and checks whether it is properly formed or not.
     */

    bool UpdateMaster(World world, bool checkRotationIndex = false);
    
    
    /**
     * Checks whether the multiblock is correctly formed or not.
     */

    bool MultiblockFormed();
    
    
    /**
     * Sets the multiblock to formed.
     */

    bool SetMultiblockFormed();


    /**
     * Sets teh multiblock to not formed.
     */

    bool SetMultiblockNotFormed();
    

    /**
     * Gets all the active slaves for this tile entity.
     */

    List<ITileEntityMultiblockSlave> GetSlaveBlocks();


    /**
     * Returns the world position in worldspace coordinates.
     */

    Vector3i ToWorldPos();
}