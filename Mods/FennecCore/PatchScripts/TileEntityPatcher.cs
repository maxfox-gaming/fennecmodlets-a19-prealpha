﻿using System.Linq;
using Mono.Cecil;
using SDX.Compiler;

public class TileEntityPatcher : IPatcherMod
{
    public bool Patch(ModuleDefinition module)
    {
        AddEnumOption(module, "TileEntityType", "BlockTransformer",                     200);
        AddEnumOption(module, "TileEntityType", "WorkstationImproved",                  201);
        AddEnumOption(module, "TileEntityType", "WorkstationPowered",                   202);
        AddEnumOption(module, "TileEntityType", "InventoryManager",                     203);
        AddEnumOption(module, "TileEntityType", "MultiblockSlave",                      204);
        AddEnumOption(module, "TileEntityType", "MultiblockSlaveLoot",                  205);
        AddEnumOption(module, "TileEntityType", "MultiblockSlaveLootInput",             206);
        AddEnumOption(module, "TileEntityType", "MultiblockSlaveLootOutput",            207);
        AddEnumOption(module, "TileEntityType", "MultiblockSlavePowered",               208);
        AddEnumOption(module, "TileEntityType", "MultiblockMaster",                     209);
        AddEnumOption(module, "TileEntityType", "MultiblockMasterTransformer",          210);
        AddEnumOption(module, "TileEntityType", "MultiblockMasterWorkstation",          211);
        AddEnumOption(module, "TileEntityType", "MultiblockMasterWorkstationPowered",   212);
        return true;
    }
    private void AddEnumOption(ModuleDefinition gameModule, string enumName, string enumFieldName, byte enumValue)
    {
        var enumType = gameModule.Types.First(d => d.Name == enumName);
        FieldDefinition literal = new FieldDefinition(enumFieldName, FieldAttributes.Public | FieldAttributes.Static | FieldAttributes.Literal | FieldAttributes.HasDefault, enumType);
        enumType.Fields.Add(literal);
        literal.Constant = enumValue;
    }

    private void AddEnumOption(ModuleDefinition gameModule, string className, string enumName, string enumFieldName, byte enumValue)
    {
        var enumType = gameModule.Types.First(d => d.Name == className).NestedTypes.First(d => d.Name == enumName);
        FieldDefinition literal = new FieldDefinition(enumFieldName, FieldAttributes.Public | FieldAttributes.Static | FieldAttributes.Literal | FieldAttributes.HasDefault, enumType);
        enumType.Fields.Add(literal);
        literal.Constant = enumValue;
    }

    public bool Link(ModuleDefinition gameModule, ModuleDefinition modModule)
    {
        return true;
    }
}