using System;
using System.Reflection;
using HarmonyLib;

/**
 * Harmony patch that allows the reading of more than the default tile entity types. 
 * Credit to SphereII for the amazing pacth.
 * Make sure to load this with the patch script else it will have an unknown type error when compiling.
 */

[HarmonyPatch(typeof(PowerItem))]
[HarmonyPatch("CreateItem")]
public class PowerItemInstantiator
{
    public static PowerItem Postfix(PowerItem __result, PowerItem.PowerItemTypes itemType)
    {
        switch (itemType)
        {
            case global::PowerItem.PowerItemTypes.WindTurbine:
                return new global::PowerWindTurbine();
            case global::PowerItem.PowerItemTypes.WaterTurbine:
                return new global::PowerWaterTurbine();
            
        }
        return __result;
    }
}