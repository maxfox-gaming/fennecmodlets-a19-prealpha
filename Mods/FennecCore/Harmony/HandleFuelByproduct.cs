﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

/**
 * Inserts call to output for handling fuel byproducts. This will add a call to our static class FuelByproductManager and
 * invoke the method HandleByproductFor(ItemValue fuelItem, ItemStack[] stackArray). 
 */

[HarmonyPatch(typeof(TileEntityWorkstation))]
[HarmonyPatch("HandleFuel")]
public class HandleFuelByproduct
{
	public static void Prefix(TileEntityWorkstation __instance, out ItemStack __state)
    {
		__state = __instance.Fuel[0].Clone();
    }

	public static void Postfix(TileEntityWorkstation __instance, ItemStack __state)
    {
		if (__instance.IsBurning && __instance.Fuel[0].count == __state.count - 1)
        {
			__instance.Output = ByproductManager.HandleFuelByproductFor(__state.itemValue, __instance.Output);
        }
    }
}





/*
public class HandleFuelByproduct
{
	public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions, ILGenerator il)
	{
		Log.Out("HandleFuelByproduct.cs: Transpiler start");
		List<CodeInstruction> code = new List<CodeInstruction>(instructions);


		Log.Out("Adding new local variable of type ItemStack[] to method...");
		LocalBuilder lb = il.DeclareLocal(typeof(ItemStack[]));
		Log.Out("Done.");


		Log.Out("Checking IL code for correct insertion location...");
		int insertionIndex = -1;
		Label label115 = il.DefineLabel();
		for (int i = 3; i < code.Count; i += 1)
		{
			if ((code[i].opcode == OpCodes.Stloc_0)
				&& (code[i - 1].opcode == OpCodes.Ldc_I4_1)
				&& (code[i - 2].opcode == OpCodes.Stfld)
				&& (((FieldInfo)code[i - 2].operand).Name == "currentBurnTimeLeft")
				&& (code[i - 3].opcode == OpCodes.Div)
			)
			{
				insertionIndex = i + 1;
				Log.Out("Found insertion index - setting to " + (i + 1).ToString());
				break;
			}
		}

		if (insertionIndex == -1)
		{
			Debug.LogError("Insertion index not found. Patch unsuccessful");
			return instructions;
		}

		bool labelled115 = false;
		Log.Out("Checking for label insertion at breakpoint...");
		for (int i = insertionIndex; i < code.Count - 3; i += 1)
		{
			if (code[i].opcode == OpCodes.Call
				&& ((MethodInfo)code[i].operand).Name == "cycleFuelStacks"
				&& (code[i + 1].opcode == OpCodes.Ldc_I4_1)
				&& (code[i + 2].opcode == OpCodes.Stloc_0)
				&& (code[i + 3].opcode == OpCodes.Ldarg_0)
			)
			{
				Log.Out("Label insertion found.");
				code[i + 3].labels.Add(label115);
				labelled115 = true;
				Log.Out("Label added for line " + (i + 3).ToString() + ".");
				break;
			}
		}

		if (!labelled115)
		{
			Log.Warning("Unable to find IL code to insert label. Patch unsuccessful.");
			return instructions;
		}

		List<CodeInstruction> instructionsToInsert = new List<CodeInstruction>()
		{
			// ldarg.0
			new CodeInstruction(OpCodes.Ldarg_0),
			// ldfld   float32 TileEntityWorkstation::currentBurnTimeLeft
			new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(teWorkstationType, burnTime)),
			// ldc.r4  0
			new CodeInstruction(OpCodes.Ldc_R4, 0f),
			// bge.un  116 (0127) ldarg.0
			new CodeInstruction(OpCodes.Bge_Un, label115),
			// ldarg.0
			new CodeInstruction(OpCodes.Ldarg_0),
			// ldfld   class ItemStack[] TileEntityWorkstation::fuel
			new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(teWorkstationType, fuel)),
			// ldc.i4.0
			new CodeInstruction(OpCodes.Ldc_I4_0),
			// ldelem.ref
			new CodeInstruction(OpCodes.Ldelem_Ref),
			// ldfld   class ItemValue ItemStack::itemValue
			new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(itemStackType, itemValue)),
			// ldarg.0
			new CodeInstruction(OpCodes.Ldarg_0),
			// ldfld class ItemStack[] TileEntityWorkstation::output
			new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(teWorkstationType, output)),
			// call class ItemStack[] [Mods] FuelByproductManager::HandleByproductFor(class ItemValue, class ItemStack[])
			new CodeInstruction(OpCodes.Call, AccessTools.Method(byproductsType, handleByproducts, new Type[] { itemValueType, itemStackArrType })),
			// stloc.1
			new CodeInstruction(OpCodes.Stloc_1, lb),
			// ldloc.1
			new CodeInstruction(OpCodes.Ldloc_1),
			// ldarg.0
			new CodeInstruction(OpCodes.Ldarg_0),
			// ldfld   class ItemStack[] TileEntityWorkstation::output
			new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(teWorkstationType, output)),
			// callvirt    instance bool[mscorlib] System.Object::Equals(object)
			new CodeInstruction(OpCodes.Callvirt, AccessTools.Method(objectType, equalsObj, new Type[] { objectType })),
			// brtrue.s    116 (0127) ldarg.0 
			new CodeInstruction(OpCodes.Brtrue_S, label115),
			// ldarg.0
			new CodeInstruction(OpCodes.Ldarg_0),
			// ldloc.1
			new CodeInstruction(OpCodes.Ldloc_1),
			// stfld   class ItemStack[] TileEntityWorkstation::output
			new CodeInstruction(OpCodes.Stfld, AccessTools.Field(teWorkstationType, output)),
			// ldarg.0
			new CodeInstruction(OpCodes.Ldarg_0),
			// ldfld   class XUiEvent_OutputStackChanged TileEntityWorkstation::OutputChanged
			new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(teWorkstationType, outputChanged)),
			// brfalse.s   116 (0127) ldarg.0 
			new CodeInstruction(OpCodes.Brfalse_S, label115),
			// ldarg.0
			new CodeInstruction(OpCodes.Ldarg_0),
			// ldfld   class XUiEvent_OutputStackChanged TileEntityWorkstation::OutputChanged
			new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(teWorkstationType, outputChanged)),
			// callvirt    instance void XUiEvent_OutputStackChanged::Invoke()
			new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(Log), nameof(Log.Out), new Type[] { typeof(string) })),
			// br.s    116 (0127) ldarg.0 
			new CodeInstruction(OpCodes.Br_S, label115),
		};

		Log.Out("Adding instructions...");
		code.InsertRange(insertionIndex, instructionsToInsert);
		Log.Out("Instructions added successfully.");


		Log.Out("Inserting label at insertion index ldarg.0");
		bool labelled40 = false;
		Label label40 = il.DefineLabel();
		if (code[insertionIndex].opcode == OpCodes.Ldarg_0)
		{
			code[insertionIndex].labels.Add(label40);
			labelled40 = true;
			Log.Out("Label added.");
		}

		if (!labelled40)
		{
			Log.Warning("Unable to add label to insertion index.");
			return instructions;
		}


		Log.Out("Finding insertion indexes for new break points...");
		int index1 = -1;
		int index2 = -1;
		Dictionary<int, CodeInstruction> newInstructionsForIndexes = new Dictionary<int, CodeInstruction>();
		for (int i = 0; i < insertionIndex - 2; i += 1)
		{
			if ((code[i].opcode == OpCodes.Ldfld)
				&& (((FieldInfo)code[i].operand).Name == "currentBurnTimeLeft")
				&& (code[i + 1].opcode == OpCodes.Ldc_R4)
				//&& ((int)code[i + 1].operand == 0)
				&& (code[i + 2].opcode == OpCodes.Bne_Un)
			)
			{
				index1 = i + 2;
				newInstructionsForIndexes.Add(index1, new CodeInstruction(OpCodes.Bne_Un_S, label40));
				Log.Out("Foud breakpoint 1 / 2");
				break;
			}
        }

		if (index1 == -1)
        {
			Log.Error("Breakpoint 1 / 2 not found.");
			return instructions;
        }

		for (int i = index1; i < insertionIndex - 2; i += 1)
        {
			if ((code[i].opcode == OpCodes.Call)
				&& (((MethodInfo)code[i].operand).Name == "getTotalFuelSeconds")
				&& (code[i + 1].opcode == OpCodes.Ldc_R4)
				//&& (((int)code[i + 1].operand) == 0)
				&& (code[i + 2].opcode == OpCodes.Ble_Un)
			)
            {
				index2 = i + 2;
				newInstructionsForIndexes.Add(index2, new CodeInstruction(OpCodes.Ble_Un_S, label40));
				Log.Out("Found breakpoint 2 / 2");
				break;
            }
        }

		if (index2 == -1)
        {
			Log.Error("Breakpoint 2 / 2 not found.");
			return instructions;
        }


		Log.Out("Changing code instructions at breakpoints.");
		foreach (KeyValuePair<int, CodeInstruction> entry in newInstructionsForIndexes)
        {
			code[entry.Key] = entry.Value;
        }
		Log.Out("Done.");


		Log.Out("Patch done.");
		return code.AsEnumerable();
	}


	private static readonly Type teWorkstationType	= typeof(TileEntityWorkstation);
	private static readonly Type itemStackType		= typeof(ItemStack);
	private static readonly Type itemStackArrType	= typeof(ItemStack[]);
	private static readonly Type itemValueType		= typeof(ItemValue);
	private static readonly Type byproductsType		= typeof(FuelByproductManager);
	private static readonly Type objectType			= typeof(object);
	private static readonly Type xuiEvent_ostType	= typeof(XUiEvent_OutputStackChanged);
	private static readonly string fuel				= "fuel";
	private static readonly string output			= "output";
	private static readonly string burnTime			= "currentBurnTimeLeft";
	private static readonly string itemValue		= "itemValue";
	private static readonly string outputChanged	= "OutputChanged";
	private static readonly string equalsObj		= nameof(object.Equals);
	private static readonly string handleByproducts	= nameof(FuelByproductManager.HandleByproductFor);
	private static readonly string outChangedInvoke = nameof(XUiEvent_OutputStackChanged.Invoke);
}
*/