﻿using HarmonyLib;

[HarmonyPatch(typeof(ItemClass))]
[HarmonyPatch("LateInit")]
public class ItemChildClassLateInit
{
    public static void Postfix(ItemClass __instance)
    {
        if (__instance is ItemClassFuelByproduct)
        {
            (__instance as ItemClassFuelByproduct).LateInit();
        }

        if (__instance is ItemClassInputByproduct)
        {
            (__instance as ItemClassInputByproduct).LateInit();
        }
    }
}