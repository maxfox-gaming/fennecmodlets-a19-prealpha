﻿using HarmonyLib;

[HarmonyPatch(typeof(XUiC_IngredientEntry))]
[HarmonyPatch("GetBindingValue")]
public class XUiC_IngredientEntryExtender
{
    public static void Postfix(XUiC_IngredientEntry __instance, ref string value, BindingItem binding)
    {
        string fieldName = binding.FieldName;
        if (fieldName == "itemname")
        {
            if (__instance.Ingredient is ItemStackFuel)
            {
                value = "[ff9999]" + value + " (" + Localization.Get("lblInFuelSlot") + ")[ffffff]";
                return;
            }
            
            if (__instance.Ingredient is ItemStackRequirement)
            {
                value = "[ffff00]" + value + " (" + Localization.Get("lblInOutputSlot") + ")[ffffff]";
                return;
            }

            if (__instance.Ingredient is ItemStackInput)
            {
                value = "[44ffff]" + value + " (" + Localization.Get("lblInInputSlot") + ")[ffffff]";
                return;
            }
        }
    }
}