﻿using HarmonyLib;
using System;
using System.Collections.Generic;

[HarmonyPatch(typeof(TileEntityWorkstation))]
[HarmonyPatch("HandleMaterialInput")]
public class HandleInputByproduct
{
    public static void Prefix(TileEntityWorkstation __instance, out ItemStack[] __state)
    {
        __state = new ItemStack[__instance.Input.Length];
        for (int i = 0; i < __instance.Input.Length; i += 1)
        {
            __state[i] = __instance.Input[i].Clone();
        }
    }


    public static void Postfix(TileEntityWorkstation __instance, ItemStack[] __state)
    {
        bool error = false;
        List<int> changedSlots;
        ItemStack[] newOutput = __instance.Output.Clone() as ItemStack[];
        if (StackCountedDown(__state, __instance.Input, out changedSlots))
        {
            foreach (int slot in changedSlots)
            {
                StackChangedState changed;
                newOutput = ByproductManager.HandleInputByproductFor(__state[slot].itemValue, __instance.Output, out changed);
                if (changed == StackChangedState.ERROR)
                {
                    error = true;
                    break;
                }
            }
        }

        if (!error)
        {
            __instance.Output = newOutput;
        }
    }



    private static bool StackCountedDown(ItemStack[] before, ItemStack[] after, out List<int> slots)
    {
        slots = new List<int>();
        int length = Math.Min(before.Length, after.Length);

        for (int i = 0; i < length; i += 1)
        {
            if (after[i].count == before[i].count - 1)
            {
                slots.Add(i);
            }
        }

        return (slots.Count > 0);
    }
}
