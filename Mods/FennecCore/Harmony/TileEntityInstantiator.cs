﻿using HarmonyLib;

/**
 * Harmony patch that allows the reading of more than the default tile entity types. 
 * Credit to SphereII for the amazing patch.
 */

[HarmonyPatch(typeof(TileEntity))]
[HarmonyPatch("Instantiate")]
public class TileEntityInstantiator
{
    public static TileEntity Postfix(TileEntity __result, TileEntityType type, Chunk _chunk)
    {
        if (__result == null)
        {
            switch (type)
            {
                case global::TileEntityType.WorkstationImproved:
                    return new TileEntityWorkstationImproved(_chunk);
                case global::TileEntityType.WorkstationPowered:
                    return new global::TileEntityWorkstationPowered(_chunk);
                case global::TileEntityType.BlockTransformer:
                    return new global::TileEntityTransformer(_chunk);
                case global::TileEntityType.InventoryManager:
                    return new global::TileEntityInventoryManager(_chunk);
                case global::TileEntityType.MultiblockSlave:
                    return new global::TileEntityMultiblockSlave(_chunk);
                case global::TileEntityType.MultiblockSlaveLoot:
                    return new global::TileEntityMultiblockSlaveLootContainer(_chunk);
                case global::TileEntityType.MultiblockMaster:
                    return new global::TileEntityMultiblockMaster(_chunk);
                case global::TileEntityType.MultiblockMasterWorkstation:
                    return new global::TileEntityMultiblockMasterWorkstation(_chunk);
                case global::TileEntityType.MultiblockMasterWorkstationPowered:
                    return new global::TileEntityMultiblockMasterWorkstationPowered(_chunk);
                case global::TileEntityType.MultiblockMasterTransformer:
                    return new global::TileEntityMultiblockMasterTransformer(_chunk);
                case global::TileEntityType.MultiblockSlavePowered:
                    return new global::TileEntityMultiblockSlavePowered(_chunk);
            }
        }
        return __result;
    }
}