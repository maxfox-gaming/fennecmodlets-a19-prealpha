﻿using System;
using System.Reflection;
using HarmonyLib;


[HarmonyPatch(typeof(TileEntityPowerSource))]
[HarmonyPatch("UpdateTick")]
public class TileEntityPowerSourceExtender
{
    public static void Postfix(TileEntityPowerSource __instance)
    {
        if (GameManager.Instance.World != null)
        {
            if (__instance.PowerItemType == global::PowerItem.PowerItemTypes.WindTurbine)
            {
                BlockWindTurbine block = GameManager.Instance.World.GetBlock(__instance.ToWorldPos()).Block as BlockWindTurbine;
                if (block == null)
                {
                    return;
                }

                Chunk chunk = GameManager.Instance.World.GetChunkFromWorldPos(__instance.ToWorldPos()) as Chunk;
                if (chunk == null)
                {
                    return;
                }

                BlockEntityData ebcd = chunk.GetBlockEntity(__instance.ToWorldPos());
                if (ebcd == null)
                {
                    return;
                }

                WindTurbineController controller = block.AddOrGetController(ebcd);
                if (controller != null)
                {
                    if (__instance.IsOn)
                    {
                        controller.SetOn();
                    }
                    else
                    {
                        controller.SetOff();
                    }
                }
            }
        }
    }
}