﻿using HarmonyLib;

/**
 * Harmony patch that allows the recipe info to show in the crafting info window.
 */

[HarmonyPatch(typeof(XUiC_RecipeEntry))]
[HarmonyPatch("GetBindingValue")]
class XUiC_RecipeEntryRecipeHack
{
	public static void Postfix(Recipe ___recipe, ref string value, BindingItem binding)
	{
		string fieldName = binding.FieldName;
		if (fieldName == "recipename")
		{
			if (___recipe == null)
            {
				return;
            }

			if ((___recipe is RecipeTransformer))
			{
				value = "[99ffff]" + value + "[ffffff]";
			}

			if (___recipe is IRecipeByproduct)
            {
				value = "[ffff99]" + value + "[ffffff]";
            }

			if (___recipe is IRecipeProbability)
            {
				double probValue = (___recipe as IRecipeProbability).ConvertToProbPerc();
				if (probValue < 100)
				{
					value += " [ff99ff](" + probValue.ToString() + "%)[ffffff]";
				}
			}


			if (___recipe.count > 1)
			{
				value += " [ffff00]x " + ___recipe.count.ToString() + "[ffffff]";
			}
		}
	}
}