﻿using HarmonyLib;

/**
 * Harmony patch that extends functionality of the workstation.
 * Powered workstations halt crafting when unpowered.
 * Multiblock workstations halt crafting when not formed.
 * Multiblock powered workstations halt crafting when not formed or not receiving power.
 */


[HarmonyPatch(typeof(TileEntityWorkstation))]
[HarmonyPatch("UpdateTick")]
public class WorkstationPoweredHack
{
    public static bool Prefix(TileEntityWorkstation __instance, World world)
    {
        bool flag1 = ((__instance as ITileEntityReceivePower) != null);
        bool flag2 = ((__instance as ITileEntityMultiblockMaster) != null);

        if (!flag1 && !flag2)
        {
            return true;
        }

        if (flag1 && flag2)
        {
            if (!(__instance as ITileEntityMultiblockMaster).MultiblockFormed())
            {
                WorkstationPoweredHack.UpdateMaster((ITileEntityMultiblockMaster)__instance);
                return false;
            }

            if (!(__instance as ITileEntityReceivePower).IsPowered(world))
            {
                return false;
            }
            return true;
        }

        if (flag1)
        {
            if (!(__instance as ITileEntityReceivePower).IsPowered(world))
            {
                return false;
            }
            return true;
        }

        if (flag2)
        {
            if (!(__instance as ITileEntityMultiblockMaster).MultiblockFormed())
            {
                WorkstationPoweredHack.UpdateMaster((ITileEntityMultiblockMaster)__instance);
                return false;
            }
            return true;
        }

        return false;
    }



    private static void UpdateMaster(ITileEntityMultiblockMaster master)
    {
        World world = GameManager.Instance.World;
        if (world != null)
        {
            switch (((TileEntity)master).GetTileEntityType())
            {
                case TileEntityType.MultiblockMasterWorkstationPowered :
                    if (master is TileEntityMultiblockMasterWorkstationPowered)
                    {
                        ((TileEntityMultiblockMasterWorkstationPowered)master).UpdateMaster(world);
                    }
                    break;
                case TileEntityType.MultiblockMasterWorkstation:
                    if (master is TileEntityMultiblockMasterWorkstation)
                    {
                        ((TileEntityMultiblockMasterWorkstation)master).UpdateMaster(world);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}