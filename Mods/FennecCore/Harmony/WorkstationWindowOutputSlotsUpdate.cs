﻿using HarmonyLib;
using System.Reflection;

/**
 * Workstation monitoring output.
 */

[HarmonyPatch(typeof(XUiC_WorkstationWindowGroup))]
[HarmonyPatch("Update")]
public class WorkstationWindowOutputSlotsUpdate
{
    public static void Postfix(XUiC_WorkstationWindowGroup __instance, XUiC_WorkstationOutputGrid ___outputWindow)
    {
        ItemStack[] WorkstationStackFromInstance = __instance.WorkstationData.GetOutputStacks();
        ItemStack[] WorkstationStackFromUI       =___outputWindow.GetSlots();
        if (!WorkstationStackFromInstance.Equals(WorkstationStackFromUI))
        {
            ___outputWindow.SetSlots(WorkstationStackFromInstance);
        }
    }
}