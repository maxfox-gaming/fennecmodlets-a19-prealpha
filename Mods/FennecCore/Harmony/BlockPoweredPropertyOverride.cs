﻿using HarmonyLib;

[HarmonyPatch(typeof(BlockPowered))]
[HarmonyPatch("Init")]
class BlockPoweredPropertyOverride
{
	/**
	 * Allows the block property MultiblockType to be usable with basic power.
	 */

    public static void Postfix(BlockPowered __instance)
    {
		if (__instance.Properties.Values.ContainsKey("MultiblockType") && __instance.Properties.Values["MultiblockType"] != "")
		{
			global::MultiblockManager.AddBlockType(__instance.Properties.Values["MultiblockType"]);
		}
	}
}
