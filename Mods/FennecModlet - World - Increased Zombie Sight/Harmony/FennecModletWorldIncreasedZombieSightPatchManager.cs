﻿using DMT;
using HarmonyLib;
using System.Reflection;

public class FennecModletWorldIncreasedZombieSightPatchManager : IHarmony
{
    public void Start()
    {
        var harmony = new Harmony(GetType().ToString());
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }
}