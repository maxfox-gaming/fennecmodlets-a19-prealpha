# FennecModlets A19 PreAlpha

## Installation
1. Clear game cache on steam. To do this, go to your Steam > Library > 7 Days to Die (right click) > Properties > Local Files > Verify Local Files
2. Create a copy of your 7 Days to Die folder (You can click Browse Local Files to view it) and call it 7 Days to Die Modded or something similar.
3. Download the DMT tool and run it.
4. Download this repo, unzip it and store the Mods foler somewhere on your PC.
5. In DMT, browse to the Mods folder and set the game folder to your copied one we made earlier.
6. Enable all the modlets (they should now appear in the menu on the left)
7. Click Build
8. Click Play.

## BUGS
Please report all bugs in the bug-reports channel. I will fix when possible and post updates for everyone to download.
